const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const path = require("path");

//Bring in database
const config = require("./config/database");

//Connect to database
mongoose
  .connect(config.database, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log("Database Connected Successfully" + config.database);
  })
  .catch(err => {
    console.log(err);
  });

//Initialize app
const app = express();

//Defining a port
const PORT = process.env.PORT || 5000;

//Defining Middlewares
app.use(cors());

//Set the static folder
app.use(express.static(path.join(__dirname, "public")));

//Bodyparser
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

app.get("/", (req, res) => {
  return res.json({ message: "This is a node role based authenticaion" });
});

//Create a custom middleware function
const checkUserType = function(req, res, next) {
  const userType = req.originalUrl.split("/")[2];
  //Bring in the passport authentication stratgy
  require("./config/passport")(userType, passport);
  next();
};

app.use(checkUserType);

//Bringing the user route
const usersRoute = require("./routes/users");
app.use("/api/users", usersRoute);

//Bringing the admin route
const adminRoute = require("./routes/admin");
app.use("/api/admin", adminRoute);

app.listen(PORT, () => console.log("server is running at port " + PORT));
